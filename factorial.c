#include <stdio.h>
#include <stdlib.h>
int main()
{
    int i = 0;
    int n;
    int factorial = 1;
    puts("* The program will compute *");
    puts("* the factorial of integer *");
    puts("please input the number n:");
    scanf("%d",&n);
    if(n < 0)
    {
        printf("Please input an integer>=0. \n");
        return 0;
    }
    if (n == 0)
    {
        printf("factorial of 0 is 1. \n");
        return 0;
    }
    i = 1;
    while (i <= n) {
        factorial = factorial *i;
        i++;
    }
    printf("factorial of %d is: %d. \ n",n,factorial);
    getch();
    return 0;

}
